all_rooms = {
    "Tavern": {
        "description": "You're in a cozy tavern warmed by an open fire." +
        "\nThere is the tavern owner sitting on a lazy wooden chair besides" +
        "\nthe counter, sleeping like there is no tomorrow. Probably were doing"
        "\nheavy lifting somewhere and ended up looking like a drunkard.",
        "exits": {"yard": "Tavern's Yard"},
        "items": {"apple": {"alias": "apple", "amount": 1, "color": "green"}, "ale": {"amount": 5}}
        },

    "Tavern's Yard": {
        "description": "You're standing outside a tavern. It's raining.",
        "exits": {"tavern": "Tavern", "gate": "Tavern's Gate"},
    },
    "Tavern's Gate": {
        "description": "You're standing next to the main gate of the tavern.",
        "exits": {"yard": "Tavern's Yard", "main road": "Main Road"}
    },
    "Main Road": {
        "description": "Far ahead",
        "exits": {"gate": "Tavern's Gate", "fountain": "Elthral Fountain"},
    },
    "Elthral Fountain": {
        "description": "Way far ahead",
        "exits": {"gate": "Main Road", "town center": "Town Center"}
    },
    "North Town Center": {
        "description": "None",
        "exits": {"town center": "Town Center"}
    },  
    "Town Center": {
        "description": "There are four routes in the town center.",
        "exits": {"north": "North Town Center", "fountain": "Elthral Fountain"}
    }
}

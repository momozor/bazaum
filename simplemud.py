#!/usr/bin/env python3

"""A simple Multi-User Dungeon (MUD) game. Players can talk to each
other, examine their surroundings and move between rooms.

Some ideas for things to try adding:
    * More rooms to explore
    * An 'emote' command e.g. 'emote laughs out loud' -> 'Mark laughs
        out loud'
    * A 'whisper' command for talking to individual players
    * A 'shout' command for yelling to players in all rooms
    * Items to look at in rooms e.g. 'look fireplace' -> 'You see a
        roaring, glowing fire'
    * Monsters to fight
    * Loot to collect
    * Saving players accounts between sessions
    * A password login
    * A shop from which to buy items

current author: Momozor
original author: Mark Frimston - mfrimston@gmail.com
"""

import time

# import the MUD server class
from mudserver import MudServer
import bauzrooms
import bauzplayers as bp
from termcolor import colored

# structure defining the rooms in the game. Try adding more rooms to the game!
rooms = bauzrooms.all_rooms

# stores the players in the game
players = {}

# start the server
mud = MudServer()

# main game loop. We loop forever (i.e. until the program is terminated)
while True:

    # pause for 1/5 of a second on each loop, so that we don't constantly
    # use 100% CPU time
    time.sleep(0.2)

    # 'update' must be called in the loop to keep the game running and give
    # us up-to-date information
    mud.update()

    # go through any newly connected players
    for id in mud.get_new_players():

        # add the new player to the dictionary, noting that they've not been
        # named yet.
        # The dictionary key is the player's id number. We set their room to
        # None initially until they have entered a name
        # Try adding more player stats - level, gold, inventory, etc
        players[id] = {
            "name": None,
            "health": 100,
            "room": None,
            "gold": 100,
            "level": 1,
            "inventory": [],
        }

        # send the new player a prompt for their name
        mud.send_message(id, colored("What is your name? ", "green"))

    # go through any recently disconnected players
    for id in mud.get_disconnected_players():

        # if for any reason the player isn't in the player map, skip them and
        # move on to the next one
        if id not in players:
            continue

        # go through all the players in the game
        for pid, pl in list(players.items()):
            # send each player a message to tell them about the diconnected
            # player
            mud.send_message(pid, colored("{} quit the game".format(
                                                        players[id]["name"]), "red"))

        # remove the player's entry in the player dictionary
        del(players[id])

    # go through any new commands sent from players
    for id, command, params in mud.get_commands():

        # if for any reason the player isn't in the player map, skip them and
        # move on to the next one
        if id not in players:
            continue

        # if the player hasn't given their name yet, use this first command as
        # their name and move them to the starting room.
        if players[id]["name"] is None:

            players[id]["name"] = command
            players[id]["room"] = "Tavern"

            # go through all the players in the game
            for pid, pl in list(players.items()):
                # send each player a message to tell them about the new player
                mud.send_message(pid, colored("\n{} entered the game\n".format(
                                                        players[id]["name"]), "green"))

            # send the new player a welcome message
            mud.send_message(id, colored("Welcome to the game, {}. ".format(
                                                           players[id]["name"]), "yellow")
                             + colored("Type 'help' for a list of commands. Have fun!", "green"))

            # send the new player the description of their current room
            mud.send_message(id, colored(rooms[players[id]["room"]]["description"], "blue"))

        # each of the possible commands is handled below. Try adding new
        # commands to the game!

        # 'help' command
        elif command == "help":

            # send the player back the list of possible commands
            mud.send_message(id, colored("Commands:", "cyan"))
            mud.send_message(id, colored("  say <message>  - Says something out loud e.g. 'say Hello'",  "green"))
            mud.send_message(id, "  look           - Examines the "
                                 + "surroundings, e.g. 'look'")
            mud.send_message(id, "  go <exit>      - Moves through the exit "
                                 + "specified, e.g. 'go outside'")
            mud.send_message(id, " show <thing> - Show item in your inventory")

        elif command == "show":
            if params == "gold":
                mud.send_message(id, colored("Your current gold is: {}".format(players[id]["gold"]), "yellow"))

            elif params is "":
                mud.send_message(id, "Must show <something>")


        elif command == "take":
            rm = rooms[players[id]["room"]]
            if players[id]["room"] == "Tavern":
                if params in rm["items"]:
                    rm["items"][params]["amount"] = rm["items"][params]["amount"] - 1

                    current_item = rm["items"][params]

                    if current_item in players[id]["inventory"]:
                        players[id][params]["amount"] = players[id][params]["amount"] + 1
                    elif current_item not in players[id]["inventory"]:
                        players[id]["inventory"].append(current_item)

                    print(players[id]["inventory"])
                    print(rm["items"])
                        



        elif command == "say":
            for pid, pl in list(players.items()):
                if players[pid]["room"] == players[id]["room"]:
                    if players[pid] is not players[id]:
                        mud.send_message(pid, "{} says: {}".format(players[id]["name"], params))
            mud.send_message(id, colored("You says: {}".format(params), "magenta"))

        # 'look' command
        elif command == "look":

            # store the player's current room
            rm = rooms[players[id]["room"]]

            # send the player back the description of their current room
            mud.send_message(id, colored(rm["description"], "blue"))

            playershere = []
            # go through every player in the game
            for pid, pl in list(players.items()):
                # if they're in the same room as the player
                if players[pid]["room"] == players[id]["room"]:
                    # ... and they have a name to be shown
                    if players[pid]["name"] is not None:
                        # add their name to the list
                        playershere.append(players[pid]["name"])

            # send player a message containing the list of players in the room
            mud.send_message(id, "Players here: {}".format(
                                                    colored(", ".join(playershere), "yellow")))

            # send player a message containing the list of exits from this room
            mud.send_message(id, "Exits are: {}".format(
                                                    colored(", ".join(rm["exits"]), "cyan")))

        # 'go' command
        elif command == "go":

            # store the exit name
            ex = params.lower()

            # store the player's current room
            rm = rooms[players[id]["room"]]

            # if the specified exit is found in the room's exits list
            if ex in rm["exits"]:

                # go through all the players in the game
                for pid, pl in list(players.items()):
                    # if player is in the same room and isn't the player
                    # sending the command
                    if players[pid]["room"] == players[id]["room"] \
                            and pid != id:
                        # send them a message telling them that the player
                        # left the room
                        mud.send_message(pid, colored("{} left via exit '{}'".format(
                                                      players[id]["name"], ex), "cyan"))

                # update the player's current room to the one the exit leads to
                players[id]["room"] = rm["exits"][ex]
                rm = rooms[players[id]["room"]]

                # go through all the players in the game
                for pid, pl in list(players.items()):
                    # if player is in the same (new) room and isn't the player
                    # sending the command
                    if players[pid]["room"] == players[id]["room"] \
                            and pid != id:
                        # send them a message telling them that the player
                        # entered the room
                        mud.send_message(pid,
                                         colored("{} arrived via exit '{}'".format(
                                                      players[id]["name"], ex), "magenta"))

                # send the player a message telling them where they are now
                mud.send_message(id, colored("You arrive at '{}'".format(
                                                          players[id]["room"]), "cyan"))

            # the specified exit wasn't found in the current room
            else:
                # send back an 'unknown exit' message
                mud.send_message(id, colored("Unknown exit '{}'".format(ex), "red"))

        # some other, unrecognised command
        else:
            # send back an 'unknown command' message
            mud.send_message(id, colored("Unknown command '{}'".format(command), "red"))
